//
//  SecondViewController.swift
//  swift-poker-screen
//
//  Created by Sanja Pantić on 22/09/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    
    var bet = 0;
    var allMoney = 0
    
    var deck2 = Array<Card>()
    var card = Card()
    
    @IBOutlet weak var betInfo: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var credit: UIButton!
    
    override func viewDidLoad() {
        
        printInfo()
        
        credit.isEnabled = false
        
    }
    
    @IBAction func low(_ sender: AnyObject) {
        
        let card = chooseRndCard()
        
        cardImage.image = card.img
        
        if card.num <= 7 && card.num > 1 {
            
            bet *= 2
            
            printInfo()
            
        } else {
            
            bet = 0
            
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func hi(_ sender: AnyObject) {
        
        let card = chooseRndCard()
        
        cardImage.image = card.img
        
        if card.num > 7 || card.num == 0 {
            
            bet *= 2
            
            printInfo()
            
        } else {
            
            bet = 0
            
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func finish(_ sender: AnyObject) {
        
        allMoney += bet
        
    }
    
    func chooseRndCard() -> Card {
        
        let fvc = FirstViewController()
        
        deck2 = fvc.makeDeck()
        
        let rd = Int(arc4random_uniform(52))
        
        return deck2[rd]
        
    }
    
    func printInfo() {
        
        betInfo.text = String(format:"BET $: %d", bet)
        
        let title = String(format:"CREDIT$: %d", allMoney)
        
        credit.setTitle(title, for: .normal)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "secondToFirstController" {
            
            if let nextViewController = segue.destination as? FirstViewController {
                
                nextViewController.allMoney = self.allMoney
                
            }
        }
    }

}
