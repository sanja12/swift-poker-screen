//
//  ViewController.swift
//  swift-poker-screen
//
//  Created by Sanja Pantić on 21/09/19.
//  Copyright © 2019 Sanja Pantić. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeDeck()
        
        gambleGame.isEnabled = false
        finishBtn.isEnabled = false
        
        bet = 0
        
        if allMoney == nil {
            
            allMoney = 1000
            
        }
        
        printMoneyInfo3()
        
        showHideHeldLbls(yesOrNo: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var cardCombination: UILabel!
    @IBOutlet weak var win: UILabel!
    @IBOutlet weak var moneyInfo: UILabel!
    @IBOutlet weak var betMoney: UILabel!

    @IBOutlet weak var card0: UIButton!
    @IBOutlet weak var card1: UIButton!
    @IBOutlet weak var card2: UIButton!
    @IBOutlet weak var card3: UIButton!
    @IBOutlet weak var card4: UIButton!
    
    @IBOutlet weak var held0: UILabel!
    @IBOutlet weak var held1: UILabel!
    @IBOutlet weak var held2: UILabel!
    @IBOutlet weak var held3: UILabel!
    @IBOutlet weak var held4: UILabel!
    
    @IBOutlet weak var deal: UIButton!
    @IBOutlet weak var finishBtn: UIButton!
    @IBOutlet weak var gambleGame: UIButton!
    @IBOutlet weak var betAllBtn: UIButton!
    @IBOutlet weak var removeMoney: UIButton!
    @IBOutlet weak var addMoney: UIButton!
    
    var bet = 0
    var allMoney: Int?
    var stake = 100
    var isFirstDeal = true
    var score = 0
    
    var deck = Array<Card>()
    var cardsForReplace = Array<UIButton>()
    var ourFiveCards = Array<Card>()
    
    @IBAction func replace0(_ sender: UIButton) {
        
        replaceOneCard(btn: sender, lbl: held0)
        
    }
    
    @IBAction func replace1(_ sender: UIButton) {
        
        replaceOneCard(btn: sender, lbl: held1)
        
    }
    
    @IBAction func replace2(_ sender: UIButton) {
        
        replaceOneCard(btn: sender, lbl: held2)
        
    }
    
    @IBAction func replace3(_ sender: UIButton) {
        
        replaceOneCard(btn: sender, lbl: held3)
        
    }
    
    @IBAction func replace4(_ sender: UIButton) {
        
        replaceOneCard(btn: sender, lbl: held4)
        
    }

    @IBAction func dealCards(_ sender: AnyObject) {
        
        gambleGame.isEnabled = false
        
        if allMoney == 0 && isFirstDeal {
            
            isMoneyZero()
            return
            
        }
        
        if isFirstDeal {
            
            allMoney! -= stake
            
            showCards()
            
            isFirstDeal = false
            disableEnableBtns(param: true)
            showHideHeldLbls(yesOrNo: false)
            
        } else {
            
            if cardsForReplace.count > 0 {
                
                replaceCards()
                cardsForReplace = Array<UIButton>()
            }
            
            isFirstDeal = true
            disableEnableBtns(param: false)
            showHideHeldLbls(yesOrNo: true)
            
        }
        
        score = calculateWin()
        
        bet = stake / 100 * score
        
        if isFirstDeal {
            
            stake = 100
            
            if score > 0 {
                
                disableEnableMainBtns(param: false)
                
            }
            
        }
        
        printMoneyInfo2()
        
        if allMoney == 0 && bet == 0 && isFirstDeal {
            
            isMoneyZero()
            
        }
        
    }
    
    @IBAction func betAll(_ sender: AnyObject) {
        
        if isMoneyZero() || !isFirstDeal {
            
            return
            
        }
        
        stake = allMoney!
        
        printMoneyInfo3()
        
    }
    
    @IBAction func finishGame(_ sender: AnyObject) {
        
        disableEnableMainBtns(param: true)
        
        allMoney! += bet
        
        bet = 0
        
        stake = 100
        
        printMoneyInfo3()
        
    }
    @IBAction func goToGambleGame(_ sender: AnyObject) {
        
        disableEnableMainBtns(param: true)
        
        printMoneyInfo3()
        
    }
    
    @IBAction func removeMoney(_ sender: AnyObject) {
        
        if isMoneyZero() || !isFirstDeal {
            
            return
            
        }
        
        if stake > 100 {
            
            stake -= 100
            
        }
        
        printMoneyInfo()
        
    }
    
    @IBAction func addMoney(_ sender: AnyObject) {
        
        if isMoneyZero() || !isFirstDeal {
            
            return
            
        }
        
        if allMoney! > stake {
            
            stake += 100
            
        }
        
        printMoneyInfo()
        
    }
    
    func printMoneyInfo() {
        
        moneyInfo.text = String(format:"CREDIT $: %i", allMoney!)
        betMoney.text = String(format:"BET $: %i", stake)
        
    }
    
    func printMoneyInfo2() {
        
        moneyInfo.text = String(format:"CREDIT $: %i", allMoney!)
        betMoney.text = String(format:"BET $: %i", stake)
        win.text = String(format:"WIN $: %i", bet)
    
    }
    
    func printMoneyInfo3() {
        
        moneyInfo.text = String(format:"CREDIT $: %i", allMoney!)
        betMoney.text = String(format:"BET $: %i", stake)
        win.text = ""
        cardCombination.text = ""
        
    }
    
    func disableEnableMainBtns(param: Bool) {
        
        gambleGame.isEnabled = !param
        finishBtn.isEnabled = !param
        deal.isEnabled = param
        
        addMoney.isEnabled = param
        removeMoney.isEnabled = param
        betAllBtn.isEnabled = param
        
    }
    
    func disableEnableBtns(param: Bool) {
    
        card0.isEnabled = param
        card1.isEnabled = param
        card2.isEnabled = param
        card3.isEnabled = param
        card4.isEnabled = param
        
        addMoney.isEnabled = !param
        removeMoney.isEnabled = !param
        betAllBtn.isEnabled = !param
       
    }
    
    func isMoneyZero() -> Bool {
    
        if allMoney! < 100 {
            
            cardCombination.text = "G A M E   O V E R"
    
            deal.isEnabled = false
            addMoney.isEnabled = false
            removeMoney.isEnabled = false
            betAllBtn.isEnabled = false
    
            return true
        }
    
        return false
    }
    
    func showCards() {
        
        ourFiveCards = Array<Card>()
    
        var indexes = Array<Int>()
        
        for i in 0..<5 {
            
            var btn = UIButton()
            
            var a = Int(arc4random_uniform(54))
    
            while (indexes.contains(a)) {
                
                a = Int(arc4random_uniform(54))
                
            }
            
            indexes.append(a)
            
            let card1 = deck[a]
            
            switch i {
            case 0:
                btn = self.card0
                break
            case 1:
                btn = self.card1
                break
            case 2:
                btn = self.card2
                break
            case 3:
                btn = self.card3
                break
            case 4:
                btn = self.card4
            default:
                break
            }
            
            let img = card1.img
            
            btn.setBackgroundImage(img, for: .normal)
            
            ourFiveCards.append(card1)
            
        }

    }
    
    func sortCards() -> Array<Card> {
        
        let retVal = ourFiveCards.sorted(by: { $0.num < $1.num })
        
        print(String(format: "Sorted cards: %d, %d, %d, %d, %d", retVal[0].num, retVal[1].num, retVal[2].num, retVal[3].num, retVal[4].num))
    
        return retVal
    
    }
    
    func checkCard(escapeNum: Int) -> Card {
        
        var rd = Int(arc4random_uniform(54))
        
        var newCard = deck[rd]
        
        var isCardInDeck = true
        
        while (isCardInDeck) {
            
            isCardInDeck = false
            
            for i in 0...(ourFiveCards.count - 1) {
                
                if i == escapeNum {
                    
                    continue
                    
                }
                
                let it = ourFiveCards[i]
                
                if newCard.num == it.num && newCard.sign == it.sign {
                  
                    isCardInDeck = true
                    
                    rd = Int(arc4random_uniform(54))
                    
                    newCard = deck[rd]
                    
                    break
                }
            }
        }
        
        print(String(format: "Test checked card %d, %@", newCard.num, newCard.sign))
        
        return newCard
        
    }
    
    func makeDeck() -> Array<Card> {
        
        for k in 1..<3 {
            
            let joker = Card()
            
            joker.num = 0
            joker.sign = "joker"
            
            let img = String(format:"joker%i", k)
            
            joker.img = UIImage(named: img)
            
            deck.append(joker)
            
        }
        
        for j in 0..<4 {
            
            var sign: String
        
            if j == 0 {
                
                sign = "H"
                
            } else if j == 1 {
                
                sign = "S"
                
            } else if j == 2 {
                
                sign = "D"
                
            } else {
                
                sign = "C"
                
            }
            
            for i in 2..<15 {
                
                let myCard = Card()
                
                myCard.num = i
                myCard.sign = sign
                
                let img = String(format:"%i%@.png", i, sign)
                
                myCard.img = UIImage(named: img)
                
                deck.append(myCard)
            }
        }
        
        let retVal = deck
        
        return retVal
    
    }

    
    func replaceCards() {
    
        for i in 0..<cardsForReplace.count {
            
            print("lala")
            
            let btn = cardsForReplace[i]
            
            var card = Card()
            
            if btn  == card0 {
                
                card = checkCard(escapeNum: 0)
                ourFiveCards[0] = card
                
            } else if btn  == card1 {
                
                card = checkCard(escapeNum: 1)
                ourFiveCards[1] = card
                
            } else if btn  == card2 {
                
                card = checkCard(escapeNum: 2)
                ourFiveCards[2] = card
                
            } else if btn  == card3 {
                
                card = checkCard(escapeNum: 3)
                ourFiveCards[3] = card
                
            } else if btn  == card4 {
                
                card = checkCard(escapeNum: 4)
                ourFiveCards[4] = card
                
            }
            
            let img = card.img
            
            btn.setBackgroundImage(img, for: .normal)
            
        }
        
    }
    
    func calculateWin() -> Int {
    
        var sortedCards = sortCards()
        
        let first = sortedCards[0]
        let second = sortedCards[1]
        let third = sortedCards[2]
        let fourth = sortedCards[3]
        let fifth = sortedCards[4]
        
        var sign = 1
        
        for i in 1..<5 {
            
            let card = sortedCards[i]
            
            if card.sign == first.sign {
                
                sign += 1
                
            }
        }
        
        // Five of a kind
        
        if first.num == 0 && second.num == third.num && third.num == fourth.num && fourth.num == fifth.num {
            
            cardCombination.text = "FIVE OF A KIND"
            
            return 2000
            
        }
        
        // Straight flush
        
        if sign == 5 && first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fourth.num == fifth.num - 1 {
            
            // Royal straight flush
            
            if first.num == 10 {
                
                cardCombination.text = "ROYAL STRAIGHT FLUSH"
                
                return 1200
                
            }
            
            cardCombination.text = "STRAIGHT FLUSH"
            
            return 1000
            
        }
        
        // Straight flush low
        
        if sign == 5 && first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fifth.num == 11 && first.num == 2 {
            
            cardCombination.text = "STRAIGHT FLUSH LOW"
            
            return 900
        }
        
        var num1 = 1
        var num2 = 1
        var num3 = 1
        var num4 = 1
        
        var cardNum = Card()
        
        for i in 2..<5 {
            
            cardNum = sortedCards[i]
            
            if i == 2 && first.num == second.num && first.sign  != "joker" {
                
                num1 += 1
                
            }
            
            if cardNum.sign != "joker" {
                
                if cardNum.num == first.num {
                   
                    num1 += 1
                    continue
                    
                }
                
                if cardNum.num == second.num {
                    
                    num2  += 1
                    continue
                    
                }
                
                if i > 2 && cardNum.num == third.num {
                    
                    num3  += 1
                    continue
                    
                }
                
                if i == 4 && cardNum.num == fourth.num {
                    
                    num4  += 1
                    
                }
            }
        }
        
        // Four of a kind
        
        if num1 == 4 || num2 == 4 {
            
            cardCombination.text = "FOUR OF A KIND"
            
            return 800
            
        }
        
        // Full house
        
        if (num1 == 3 && num2 == 2) || (num1 == 2 && num2 == 3) || (num1 == 2 && num3 == 3) {
            
            cardCombination.text = "FULL HOUSE"
            
            return 700
        }
        
        // Flush
        
        if sign == 5 {
            
            cardCombination.text = "FLUSH"
            
            return 600
            
        }
        
        // Straight
        
        if first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fourth.num == fifth.num - 1 {
            
            cardCombination.text = "STRAIGHT"
            
            return 500
            
        }
        
        // Straight low
        
        if first.num == second.num - 1 && second.num == third.num - 1 && third.num == fourth.num - 1 && fifth.num == 11 && first.num == 2 {
            
            cardCombination.text = "STRAIGHT LOW"
            
            return 400
            
        }
        
        // Three of a kind
        
        if num1 == 3 || num2 == 3 || num3 == 3 {
            
            cardCombination.text = "THREE OF A KIND"
            
            return 300
            
        }
        
        // Two pair
        
        if (num1 == 2 && num2 == 2) || (num1 == 2 && num3 == 2) || (num1 == 2 && num4 == 2) || (num2 == 2 && num3 == 2) || (num2 == 2 && num4 == 2) || (num3 == 2 && num4 == 2) {
            
            cardCombination.text = "TWO PAIR"
            
            return 200
            
        }
        
        // One pair
        
        if num1 == 2 || num2 == 2 || num3 == 2 || num4 == 2 {
            
            cardCombination.text = "ONE PAIR"
            
            return 100
        }
        
        cardCombination.text = ""
        
        return 0
    
    }
    
    func showHideHeldLbls(yesOrNo: Bool) {
        
        held0.isHidden = yesOrNo
        held1.isHidden = yesOrNo
        held2.isHidden = yesOrNo
        held3.isHidden = yesOrNo
        held4.isHidden = yesOrNo
        
    }
   
    func replaceOneCard(btn: UIButton, lbl: UILabel) {
    
        if !cardsForReplace.contains(btn) {
            
            cardsForReplace.append(btn)
            lbl.isHidden = true
            
        } else {
            
            let index = cardsForReplace.index(of: btn)
            cardsForReplace.remove(at: index!)
            
            lbl .isHidden = false
            
        }
    
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "firstToSecondController" {
            
            if let nextViewController = segue.destination as? SecondViewController {
                
                nextViewController.bet = self.bet
                
                nextViewController.allMoney = self.allMoney!
                
            }
        }
    }
}

